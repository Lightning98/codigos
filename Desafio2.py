class DadosIguais:
    def __init__(self, telefone, telefoneCelular, email, senha):
        self.telefone = telefone
        self.telefoneCelular = telefoneCelular
        self.email = email
        self.senha = senha

class Endereco:
    def __init__(self, CEP, endereco, numero, complemento, infoReferencia, bairro, cidade, estado):
        self.CEP = CEP
        self.endereco = endereco
        self.numero = numero
        self.complemento = complemento
        self.infoReferencia = infoReferencia
        self.bairro = bairro
        self.cidade = cidade
        self.estado = estado

class PessoaFisica(DadosIguais):
    def __init__(self, nomeCompleto, sexo, CPF, dataNascimento, apelido, telefone, telefoneCelular, email, senha):
        super().__init__(telefone, telefoneCelular, email, senha)
        self.nomeCompleto = nomeCompleto
        self.sexo = sexo
        self.CPF = CPF
        self.dataNascimento = dataNascimento
        self.apelido = apelido

    def ofertas(self):
        oferAmaricanasEmail = True
        oferAmericanasCel = True
        oferOutletEmail = True

class PessoaJuridica(DadosIguais, Endereco):
    def __init__(self, razaoSocial, CNPJ, inscEstadual, responsavel, telefone, telefoneCelular, email, senha, CEP, endereco, numero, complemento, infoReferencia, bairro, cidade, estado):
        super(DadosIguais).__init__(telefone, telefoneCelular, email, senha)
        super(Endereco).__init__(CEP, endereco, numero, complemento, infoReferencia, bairro, cidade, estado)
        self.razaoSocial = razaoSocial
        self.CNPJ = CNPJ
        self.inscEstadual = inscEstadual
        self.responsavel = responsavel

    def infoTributaria(self):
        contribuinteICMS = False
        naoContribuinte = False
        isentoInscEstatual = False

    def ofertas(self):
        oferAmaricanasEmail = True
        oferAmericanasCel = True
        oferOutletEmail = False
