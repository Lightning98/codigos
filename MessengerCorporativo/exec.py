import sqlite3
import os
import time
import datetime



connection = sqlite3.connect('BancoDados.db')

connection.execute('''CREATE TABLE IF NOT EXISTS DADOS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                LOGIN TEXT NOT NULL UNIQUE,
                SENHA TEXT NOT NULL,
                MENSAGEM TEXT NULL);''')

connection.execute('''CREATE TABLE IF NOT EXISTS mensagens
                (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                mensagem TEXT NOT NULL,
                dataHoraMensagem TEXT NULL,
                IDRemetente INTEGER NOT NULL,
                IDDestino INTEGER NOT NULL,
                nomeRemetente TEXT NOT NULL)''')

#connection.execute("INSERT INTO DADOS (LOGIN, SENHA) VALUES ('admin', '123')")
connection.commit()

def entrada():
    while True:
        global userLogado
        global userLogadoID
        os.system('cls')
        print("Bem-Vindo! Caso queira sair, pressione [ENTER]\n")
        login = input("Login: ")
        if(login == ""):
            connection.close()
            break
        else:
            senha = input("Senha: ")
            tabela = connection.execute("SELECT * FROM DADOS")
            for dados in tabela:
                if ((login == dados[1]) and (senha == dados[2])):
                    userLogado = dados[1]
                    userLogadoID = dados[0]
                    if((dados[1] == 'admin') and (dados[2] == '123')):
                        print("\nEntrando...")
                        time.sleep(1)
                        os.system("cls")
                        adminView()
                    else:
                        print("\nEntrando...")
                        time.sleep(1)
                        os.system("cls")
                        userView()

def criarUsuario():
    while True:
        print(">> Criar novo usuário\n")
        login = input("Digite novo Login: ")
        senha = input("Ditite nova Senha: ")
        try:
            connection.execute("INSERT INTO DADOS (LOGIN, SENHA) VALUES (?, ?)" , (login, senha))
            connection.commit()

            print("\nNovo usuário criado com sucesso.")
            time.sleep(3)
            os.system("cls")
            break
        except sqlite3.IntegrityError:
            print("Usuário já existe.")
            input()
            os.system("cls")


def verificar(nome):
    tabela = connection.execute("SELECT * FROM DADOS")
    for dados in tabela:
        if (nome == dados[1]):
            return True

def deletarUsuario():
    os.system("cls")
    print(">> Remover usuário:\n")
    nome = input("Digite o nome do usuário: ")
    if(verificar(nome) == True):
        connection.execute("DELETE FROM DADOS where LOGIN = ?",(nome,))
        connection.commit()
        print("\nUsuário excluido.")
        time.sleep(1)
        os.system("cls")
    else:
        print("\nUsuário não existe.")
        time.sleep(1)
        os.system("cls")

def cadastroMsg():
    os.system("cls")
    horaData = time.strftime("%d/%m/%y - %X")
    print(">> Enviar mensagem: \n")
    user = int(input("ID do destinatário: "))
    tabela = connection.execute("SELECT * FROM DADOS")
    for dados in tabela:
        if (user == dados[0]):
            msg = input("Digite sua mensagem: ")
            tabelaMsg = connection.execute("INSERT INTO mensagens(mensagem,dataHoraMensagem,IDRemetente,IDDestino,nomeRemetente)\
            VALUES(?,?,?,?,?)",(msg,horaData,userLogadoID,user,userLogado))
            connection.commit()
            input("\nMensagem enviada.")
            os.system("cls")

def pesquisaUsuario():
    os.system('cls')
    tabela = connection.execute("SELECT * from DADOS")
    for linha in tabela:
        print("------------------")
        print("ID = ", linha[0])
        print("NOME = ", linha[1])
        print("EMAIL = ", linha[2])
        print("------------------")
        input("\n[ENTER] Próxmio")
        connection.commit()
        os.system('cls')

def minhasMensagens():
    os.system("cls")
    tabelaMsg = connection.execute("SELECT * FROM mensagens")
    for dados in tabelaMsg:
        if dados[4] == userLogadoID:
            print(">> Mensagens recebidas: \n")
            print("Recebida de: %s ID: %i\n  %s\n" % (dados[5],dados[3],dados[2]))
            print("-%s" % dados[1])
            input("")
            os.system("cls")

def adminView():
    while True:
        print("======= ADMIN =======\n")
        print("1. Criar novo usuário\
             \n2. Alterar usuários\
             \n3. Pesquisar usuários\
             \n4. Deletar usuários\
             \n5. Enviar mensagem\
             \n6. Ver mensagens\
             \n0. Reiniciar")

        try:
            acao = int(input("\n>> "))

            if acao == 1:
                os.system("cls")
                criarUsuario() #OK
            elif acao == 2:
                os.system("cls")
                entrada() #OK
            elif acao == 3:
                pesquisaUsuario() #OK
            elif acao == 4:
                deletarUsuario() #OK
            elif acao == 5:
                cadastroMsg() #OK
            elif acao == 6:
                minhasMensagens() #OK
            elif acao == 0:
                os.system("cls")
                break
            else:
                os.system("cls")
                adminView()
        except:
            os.system("cls")
            adminView()

def userView():
    while True:
        print("======= USER =======\n")
        print("1. Enviar mensagem\
             \n2. Minhas mensagens\
             \n0. Sair")

        try:
            acao = int(input("\n>> "))
            if acao == 1:
                os.system("cls")
                cadastroMsg()
            elif acao == 2:
                os.system("cls")
                minhasMensagens()
            elif acao == 0:
                os.system("cls")
                break
            else:
                os.system("cls")
                userView()
        except:
            os.system("cls")
            userView()


entrada()
