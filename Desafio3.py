import os

class Conta:
    def __init__(self, numConta, saldoConta):
        self.numConta = numConta
        self.saldoConta = saldoConta
        self.limiteConta = 5000
        self.movimentacoes = []

    def infoDados(self):
        return(self.numConta, self.saldoConta)

    def sacar(self,valor):
        if(self.limiteConta > valor <= self.saldoConta):
            self.saldoConta -= valor
            input("Você sacou R$%i.\nNovo saldo R$%i" % valor,self.saldoConta)
            return valor
        else:
            input("Valor excede o limite da conta ou saldo disponivel")

    def depositar(self,valor):
        self.saldoConta += valor
        input("Você depositou R$%i.\nNovo saldo R$%i" % valor,self.saldoConta)
        return valor

class Banco(Conta):
    def __init__(self):
        self.contasBanco = [contasBanco]
        super().__init__(numConta,saldoConta)


vetContas = []
def criarConta():
    os.system("cls")
    numeroContas = input("Número da Conta: ")
    depositoInicial = int(input("Deposito inicial: "))
    contas = Conta(numConta = numeroContas, saldoConta = depositoInicial)
    vetContas.append(contas)
    input("Conta criada com sucesso.")

def excluirConta():
    acao = input("Número da Conta: ");
    for conta in vetContas:
        if(acao == conta.numConta):
            vetContas.remove(conta)
            input("Conta removida.");

def verificar():
    for i in vetContas:
        print(i.infoDados())
        input("")

def saque():
    acao = input("Número da Conta: ");
    for conta in vetContas:
        if(acao == conta.numConta):
            print("Sua conta tem um saldo de R$%i" % conta.saldoConta)
            valor = int(input("Qual o valor do saque? n--> "))
            conta.sacar(valor)
        else:
            input("Conta não existe.")

def deposito():
    acao = input("Número da Conta: ");
    for conta in vetContas:
        if(acao == conta.numConta):
            valor = int(input("Qual o valor do deposito?\n--> "))
            conta.depositar(valor)
        else:
            input("Conta não existe.")

def emitirSaldoExtrato():
    acao = input("Número da Conta: ");
    for conta in vetContas:
        if(acao == conta.numConta):
            input("Seu saldo é R$%i" % conta.saldoConta)


def menu():
    acao = -1;
    while acao != 0:
        os.system("cls")
        try:
            print("Banco\n")
            print("1. Criar Conta")
            print("2. Excluir Conta")
            print("3. Sacar")
            print("4. Depositar")
            print("5. Saldo e Extrato")
            print("6. Transferência")
            print("0. Sair")


            acao = int(input("\n--> "))
            if(acao == 1):
                criarConta()
            elif(acao == 2):
                excluirConta()
            elif(acao == 3):
                saque()
            elif(acao == 4):
                deposito()
            elif(acao == 5):
                emitirSaldoExtrato()
            elif(acao == 6):
                transfContas()
            elif(acao == 7):
                verificar()
            else:
                os.system("cls");
        except:
            os.system("cls")
menu();
